#include "../include/image_service.h"

struct image create_image(const uint32_t width, const uint32_t height) {
    struct image newImage = {
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * width * height)
    };

    return newImage;
}

void delete_image(struct image *image) {
    free(image->data);
}

struct image rotate_image(const struct image source) {
    if (source.data == NULL) {
        return (struct image) {
            .width = source.width,
            .height = source.height,
            .data = NULL
        };
    }

    struct pixel* pixels = malloc(sizeof(struct pixel) * source.width * source.height);


    if(!pixels){
        return (struct image){
            .width = 0,
            .height = 0,
            .data = NULL
        };
    }


    for (uint32_t i = 0; i < source.height; ++i) {
        for (uint32_t j = 0; j < source.width; ++j) {
            pixels[source.height * j + (source.height - 1 - i)] = source.data[i * source.width + j];
        }
    }

    return (struct image) { 
        .width = source.height,
        .height = source.width,
        .data = pixels
    };
}
