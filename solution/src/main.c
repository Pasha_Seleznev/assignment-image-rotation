#include "../include/bmp_service.h"
#include "../include/image_service.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv) {

    if(argc!=3){
        return 1;
    }

    const char* input_file_name = argv[1];
    const char* output_file_name = argv[2];


    struct image image = { 0, 0, NULL };
    enum read_status read_status = from_bmp(input_file_name, &image);

    if (read_status != READ_OK) {
        return read_status;
    }

    struct image rotated = rotate_image(image);
    enum write_status write_status = to_bmp(output_file_name, &rotated);

    if (write_status != WRITE_OK) {
        return write_status;
    }

    delete_image(&rotated);
    delete_image(&image);

    return 0;
}
