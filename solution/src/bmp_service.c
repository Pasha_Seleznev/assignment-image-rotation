#include "../include/bmp_service.h"
#define BfType 19778
#define BiSize 40
#define BiXPelsPerMeter 2834
#define BiYPelsPerMeter 2834
#define BiBitCount 24

const char* read_status_string[] = {
        [READ_INVALID_IMPORTANT_BITS] = "Invalid important bits",
        [READ_INVALID_EMPTY_BITS] = "Invalid empty bits",
        [READ_INVALID_HEADER] = "Invalid header",
};

const char* write_status_string[] = {
        [WRITE_ERROR] = "Write error\n"
};

void print_header(struct bmp_header* header) {
    printf("Header:\n");
    printf("  bfType:    %hu, must be 19778\n", header->bfType);
    printf("  bfileSize:  %u (file's size)\n", header->bfileSize);
    printf("  bfReserved: %u, must be 0\n", header->bfReserved);
    printf("  bOffBits: %u, at least 54 <---- \n", header->bOffBits);
    printf("  biSize: %u, must be 40\n", header->biSize);
    printf("  biWidth: %u, <---- \n", header->biWidth);
    printf("  biHeight: %u, <----\n", header->biHeight);
    printf("  biPlanes: %hu, must be 1\n", header->biPlanes);
    printf("  biBitCount: %hu, must be 24 check it!\n", header->biBitCount);
    printf("  biCompression: %u, usually 0\n", header->biCompression);
    printf("  biSizeImage: %u, <----- (file size without header) [can be 0]\n", header->biSizeImage);
    printf("  biXPelsPerMeter: %u, not important (horizontal resolution)\n", header->biXPelsPerMeter);
    printf("  biYPelsPerMeter: %u, not important (vertical resolution)\n", header->biYPelsPerMeter);
    printf("  biClrUsed: %u,  color indexes (0 - all are used)\n", header->biClrUsed);
    printf("  biClrImportant: %u, important color indexes (0 - all are important)\n", header->biClrImportant);
}

enum read_status from_bmp(const char* file_name, struct image* img) {
    struct bmp_header header = { 0 };
    FILE* input = NULL;
    open_file(&input, file_name, "rb");

    enum read_status status = read_header(input, &header);
    if (status != READ_OK) {
        close_file(input);
        printf("%s", read_status_string[status]);
        return status;
    }

    *img = create_image(header.biWidth, header.biHeight);

    status = read_pixels(input, img);
    if (status != READ_OK) {
        close_file(input);
        printf("%s", read_status_string[status]);
        return status;
    }

    close_file(input);
    return READ_OK;
}

enum write_status to_bmp(const char* file_name, const struct image* image) {
    FILE* output = NULL;
    open_file(&output, file_name, "wb");

    enum write_status status = make_header(output, image->width, image->height);
    if (status != WRITE_OK) {
        close_file(output);
        printf("%s", write_status_string[status]);
        return status;
    }

    status = write_pixels(output, image);
    if (status != WRITE_OK) {
        close_file(output);
        printf("%s", write_status_string[status]);
        return status;
    }

    close_file(output);
    return WRITE_OK;
}

enum read_status read_header(FILE* file, struct bmp_header* header) {
    if (fread(header, sizeof(struct bmp_header), 1, file) != 1) {
        return READ_INVALID_HEADER;
    }

    return READ_OK;
}

enum write_status make_header(
    FILE* const out,
    const uint32_t width,
    const uint32_t height
) {
    uint8_t padding = get_padding(width);
    const uint32_t image_size = (uint32_t)((sizeof(struct pixel) * width + padding) * height);
    struct bmp_header new_header = {
            .bfType = BfType,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BiSize,
            .biPlanes = 1,
            .biCompression = 0,
            .biXPelsPerMeter = BiXPelsPerMeter,
            .biYPelsPerMeter = BiYPelsPerMeter,
            .biClrUsed = 0,
            .biClrImportant = 0,
            .biBitCount = BiBitCount,
            .biSizeImage = image_size,
            .biWidth = width,
            .biHeight = height,
            .bfileSize = (uint32_t)(sizeof(struct bmp_header) + image_size)
    };

    if (fwrite(&new_header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}
