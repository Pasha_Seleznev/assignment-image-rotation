#include "../include/bmp_repository.h"

enum open_code open_file(FILE** file, const char* file_name, const char* mode) {
    if (!file_name) {
        return INCORRECT_FILE_NAME_TO_OPEN;
    }

    *file = fopen(file_name, mode);
    if (!*file) {
        return OPEN_ERROR;
    }

    return OPEN_OK;
}

enum close_code close_file(FILE* const file) {
    if (file != NULL) {
        fclose(file);
        return CLOSE_OK;
    } else {
        return CLOSE_ERROR;
    }
}

enum read_status read_pixels(FILE *file, struct image *image) {
    const uint32_t width = image->width, height = image->height;
    const uint8_t padding = get_padding(width);

    for (uint32_t i = 0; i < height; ++i) {
        if (fread(image->data + i * width, sizeof(struct pixel), width, file) != width) {
            return READ_INVALID_IMPORTANT_BITS;
        }

        if (fseek(file, padding, SEEK_CUR) != 0) {
            return READ_INVALID_EMPTY_BITS;
        }
    }
    return READ_OK;
}

enum write_status write_pixels(FILE *file, struct image const *image) {
    const uint32_t width = image->width, height = image->height;
    const uint8_t padding = get_padding(width);

    for (uint32_t i = 0; i < height; ++i) {
        if (fwrite(image->data + i * width, sizeof(struct pixel), width, file) != width) {
            return WRITE_ERROR;
        }

        for (uint32_t j = 0; j < padding; ++j) {
            char trash = 0;
            if (fwrite(&trash, 1, 1, file) != 1) {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

uint8_t get_padding(const uint32_t width) {
    if (width % 4 == 0) {
        return 0;
    }

    return 4 - ((width * sizeof(struct pixel)) % 4);
}
