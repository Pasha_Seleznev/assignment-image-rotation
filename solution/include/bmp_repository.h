#include "image_service.h"
#include <inttypes.h>
#include <stdio.h>

enum open_code {
    OPEN_OK = 0,
    OPEN_ERROR,
    INCORRECT_FILE_NAME_TO_OPEN,
};

enum close_code {
    CLOSE_OK = 0,
    CLOSE_ERROR,
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_IMPORTANT_BITS,
    READ_INVALID_EMPTY_BITS,
    READ_INVALID_HEADER,
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
};

enum open_code open_file(FILE** file, const char* file_name, const char* mode);

enum close_code close_file(FILE* file);

enum read_status read_pixels(FILE *file, struct image *image);

enum write_status write_pixels(FILE *file, const struct image *image);

uint8_t get_padding(uint32_t width);
