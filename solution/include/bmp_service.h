#include "bmp_repository.h"
#include "image_service.h"
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType; // File's type (for BMP - 19778 (-> "BM"))
    uint32_t bfileSize; // File's size (bytes)
    uint32_t bfReserved; // Reserved bytes (must be 0)
    uint32_t bOffBits; // Offset from beggining of file
    uint32_t biSize; // Size of info header (40 bytes)
    uint32_t biWidth; // Width (pixels)
    uint32_t biHeight; // Height (pixels)
    uint16_t biPlanes; // Count of color planes (must be 1)
    uint16_t biBitCount; // Count of bits in pixel (usually 24)
    uint32_t biCompression; // Compression type (usually 0 (-> no compression))
    uint32_t biSizeImage;  // Size of raster data (file size without header) [can be 0]
    uint32_t biXPelsPerMeter; // Horizontal resolution (count of pixels in metr)
    uint32_t biYPelsPerMeter; // Vertical resolution (count of pixels in metr)
    uint32_t biClrUsed; // Count of color indexes (0 - all are used)
    uint32_t biClrImportant; // Count of important color indexes (0 - all are important)
};
#pragma pack(pop)


void print_header(struct bmp_header* header);

enum write_status to_bmp(const char* file_name, const struct image *image);

enum read_status from_bmp(const char* file_name, struct image* img);

enum read_status read_header(FILE *file, struct bmp_header *header);

enum write_status make_header(FILE* const out, const uint32_t width, const uint32_t height);
