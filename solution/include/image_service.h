#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_MANAGER_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_MANAGER_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint32_t width, height;
    struct pixel *data;
};

struct image create_image(uint32_t width, uint32_t height);

void delete_image(struct image *image);

struct image rotate_image(struct image source);

#endif
